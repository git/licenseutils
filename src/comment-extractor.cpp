/*  Copyright (C) 2017 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/

#include <config.h>
#include <iostream>
#include <string.h>
#include <fstream>
#include "srchilite/langdefmanager.h"
#include "srchilite/regexrulefactory.h"
#include "srchilite/sourcehighlighter.h"
#include "srchilite/formattermanager.h"
#include "srchilite/formatterparams.h"
#include "srchilite/highlightstate.h"

using namespace std;

bool stop_the_madness = false;
//i adapted code from the gnu source highlight manual to make this.
//https://www.gnu.org/software/src-highlite/source-highlight-lib.html
class M4InfoFormatter: public srchilite::Formatter
{
  // the language element represented by this formatter
  std::string elem;
  int line_number;
  int prev_line_number;
  bool found;
  int src_row;
  int src_col;
  int dst_row;
  int dst_col;
  bool m4hack;

public:
  M4InfoFormatter(const std::string &elem_ = "normal") :
    elem(elem_) {line_number = 0; prev_line_number = -1; found = false;
    src_row = -1; src_col = -1; dst_row = -1; dst_col = -1; m4hack = false;}

  bool foundLine() {return found;}
  void setLineNo (size_t num) {line_number = num;}
  void getCommentBounds (int &s_row, int &s_col, int &d_row, int &d_col)
    {s_row = src_row; s_col = src_col; d_row = dst_row; d_col = dst_col;}
  void setM4hack() {m4hack = true;}

  virtual void format(const std::string &ns,
                      const srchilite::FormatterParams *params = 0) {
    if (stop_the_madness)
      return;
    std::string s = ns;
    // do not print anything if normal or string to format is empty
    bool hacked = false;
    if (m4hack)
      {
        if (elem == "keyword" && s == "dnl" )
          {
            if (src_row == -1 && src_col == -1)
              {
            src_row = line_number;
            src_col = params->start;
              }
            s = "\ndnl";
            hacked = true;
            elem = "comment";
          }
        if (elem == "comment" && s == "#")
          s = "\n#";
      }

    if (elem == "normal" && ns != " ")
      {
        /*i can't figure out how to stop the main formatter */
        stop_the_madness = true;
      }

    if (elem != "normal" || !s.size())
      {
        if (!found && (elem == "comment" || elem == "url"))
          {
            if (!hacked)
              {
            src_row = line_number;
            src_col = params->start;
              }
          }
        if (!hacked)
          found = true;
      }

    if (elem == "url" && s.size())
      {
        if (params)
          {
            if (params->start == 0)
              s = "\n" + s;
          }
      }

    if (elem == "comment" || elem == "url" || !s.size())
      {
        if (params)
          {
            if (line_number != prev_line_number && prev_line_number != -1)
              {
                if (!m4hack)
                  {
                    if (line_number - prev_line_number == 1)
                      std::cout << std::endl;
                    else
                      std::cout << std::endl << std::endl;
                  }
              }
            std::cout << s ;
            dst_row = line_number;
            dst_col = params->start + ns.length();
          }
      }
    prev_line_number = line_number;
    if (m4hack && hacked)
      {
        dst_row = line_number;
        dst_col = params->start + s.length();
        elem = "keyword";
      }

  }
};
class PHPInfoFormatter: public srchilite::Formatter
{
  // the language element represented by this formatter
  std::string elem;
  int line_number;
  int prev_line_number;
  bool found;
  int src_row;
  int src_col;
  int dst_row;
  int dst_col;

public:
  PHPInfoFormatter(const std::string &elem_ = "normal") :
    elem(elem_) {line_number = 0; prev_line_number = -1; found = false;
    src_row = -1; src_col = -1; dst_row = -1; dst_col = -1;}

  bool foundLine() {return found;}
  void setLineNo (size_t num) {line_number = num;}
  void getCommentBounds (int &s_row, int &s_col, int &d_row, int &d_col)
    {s_row = src_row; s_col = src_col; d_row = dst_row; d_col = dst_col;}

  virtual void format(const std::string &ns,
                      const srchilite::FormatterParams *params = 0) {
    std::string s = ns;
    if (stop_the_madness)
      return;
    // do not print anything if normal or string to format is empty
    if (elem == "normal" && ns != " ")
      {
        if (ns.substr (0, 5) != "<?php")
        /*i can't figure out how to stop the main formatter */
          stop_the_madness = true;
        else
          std::cout << ns << std::endl;
      }

    if (elem != "normal" || !s.size())
      {
        if (!found && (elem == "comment" || elem == "url"))
          {
            src_row = line_number;
            src_col = params->start;
          }
        found = true;
      }

    if ((elem == "url" || elem == "type") && s.size())
      {
        if (params)
          {
            if (params->start == 0)
              s = "\n" + s;
          }
      }

    if (elem == "comment" || elem == "url" || elem == "type" || !s.size())
      {
        if (params)
          {
            if (line_number != prev_line_number && prev_line_number != -1)
              {
                if (line_number - prev_line_number == 1)
                  std::cout << std::endl;
                else
                  std::cout << std::endl << std::endl;
              }
            std::cout << s ;
            dst_row = line_number;
            dst_col = params->start + ns.length();
          }
      }
    prev_line_number = line_number;
  }
};


class InfoFormatter: public srchilite::Formatter
{
  // the language element represented by this formatter
  std::string elem;
  int line_number;
  int prev_line_number;
  bool found;
  int src_row;
  int src_col;
  int dst_row;
  int dst_col;

public:
  InfoFormatter(const std::string &elem_ = "normal") :
    elem(elem_) {line_number = 0; prev_line_number = -1; found = false;
    src_row = -1; src_col = -1; dst_row = -1; dst_col = -1;}

  bool foundLine() {return found;}
  void setLineNo (size_t num) {line_number = num;}
  void getCommentBounds (int &s_row, int &s_col, int &d_row, int &d_col)
    {s_row = src_row; s_col = src_col; d_row = dst_row; d_col = dst_col;}

  virtual void format(const std::string &ns,
                      const srchilite::FormatterParams *params = 0) {
    if (stop_the_madness)
      return;
    std::string s = ns;
    //printf ("elem is '%s', s is '%s'\n", elem.c_str(), s.c_str());
    // do not print anything if normal or string to format is empty
    if (elem == "normal" && ns != " ")
      {
        /*i can't figure out how to stop the main formatter */
        stop_the_madness = true;
        return;
      }

    if (elem != "normal" || !s.size())
      {
        if (!found && (elem == "comment" || elem == "url"))
          {
            src_row = line_number;
            src_col = params->start;
          }
        found = true;
      }

    if ((elem == "url") && s.size())
      {
        if (params)
          {
            if (params->start == 0)
              s = "\n" + s;
          }
      }

    if (elem == "comment" || elem == "url" || !s.size())
      {
        if (params)
          {
            if (line_number != prev_line_number && prev_line_number != -1)
              {
                if (line_number - prev_line_number == 1)
                  std::cout << std::endl;
                else
                  std::cout << std::endl << std::endl;
              }
            std::cout << s ;
            dst_row = line_number;
            dst_col = params->start + ns.length();
          }
      }
    prev_line_number = line_number;
  }
};

// shared pointer for InfoFormatter
typedef boost::shared_ptr<InfoFormatter> InfoFormatterPtr;
typedef boost::shared_ptr<M4InfoFormatter> M4InfoFormatterPtr;
typedef boost::shared_ptr<PHPInfoFormatter> PHPInfoFormatterPtr;

int
m4_processor (std::string filename, std::string langfile, std::string outfilename, bool boilerplate, int stats)
{
 srchilite::RegexRuleFactory ruleFactory;
 srchilite::LangDefManager langDefManager (&ruleFactory);

 boost::shared_ptr<srchilite::HighlightState> p = NULL;
 try
   {
     char *dir = getenv ("LU_LANGFILE_DIR");
     if (dir)
       p = langDefManager.getHighlightState(dir, langfile);
     else
       p = langDefManager.getHighlightState(LICENSEUTILS_DATADIR "/lang-files",
                                            langfile);
   } catch (std::exception &ex)
   {
     fprintf (stderr," couldn't read langfile '%s'\n", langfile.c_str());
     exit (1);
   }
 srchilite::SourceHighlighter highlighter (p);

 string line;
 int lineNo = 0;
 ifstream infile;
 infile.open (filename.c_str());
 srchilite::FormatterManager formatterManager
         (M4InfoFormatterPtr(new M4InfoFormatter));
 M4InfoFormatterPtr keywordFormatter(new M4InfoFormatter("keyword"));
 keywordFormatter->setM4hack();
 formatterManager.addFormatter("keyword", keywordFormatter);

 M4InfoFormatterPtr commentFormatter(new M4InfoFormatter("comment"));
 commentFormatter->setM4hack();
 formatterManager.addFormatter("comment", commentFormatter);

 M4InfoFormatterPtr linkFormatter(new M4InfoFormatter("url"));
 formatterManager.addFormatter("url", linkFormatter);

 M4InfoFormatterPtr todoFormatter(new M4InfoFormatter("todo"));
 formatterManager.addFormatter("todo", todoFormatter);

 M4InfoFormatterPtr typeFormatter(new M4InfoFormatter("type"));
 formatterManager.addFormatter("type", typeFormatter);

 M4InfoFormatterPtr preprocFormatter(new M4InfoFormatter("preproc"));
 formatterManager.addFormatter("preproc", preprocFormatter);
 highlighter.setFormatterManager(&formatterManager);

 // normally we stop when any of these find something.
 M4InfoFormatterPtr ar[] =
   {
     keywordFormatter,
     preprocFormatter,
     NULL
   };
 srchilite::FormatterParams params;
 highlighter.setFormatterParams(&params);

 std::ofstream out(outfilename.size () > 0 ? outfilename.c_str() : "");
 std::streambuf *coutbuf = NULL;
 if (outfilename.size () > 0)
   {
     coutbuf = std::cout.rdbuf(); 
     std::cout.rdbuf(out.rdbuf());
   }
 // we now highlight a line a time
 while (getline(infile, line))
   {
     params.start = 0; // reset position counter within a line

     commentFormatter->setLineNo(lineNo);
     keywordFormatter->setLineNo(lineNo);
     try 
       {
         highlighter.highlightParagraph(line);
       } catch (std::exception &e)
     {
       //okay all bets are off
       break;
     }
     lineNo++;
     if (boilerplate)
       {
         if (stop_the_madness)
           break;
         bool found = false;
         int count = 0;
         while (ar[count])
           {
             if (ar[count]->foundLine())
               {
                 found = true;
                 break;
               }
             count++;
           }
         if (found)
           break;
       }
   }
 if (commentFormatter->foundLine())
   {
     std::cout << std::endl;
     if (stats)
       {
         int sr, sc, dr, dc;
         commentFormatter->getCommentBounds(sr, sc, dr, dc);
         //printf("C %d %d %d %d\n", sr, sc, dr, dc);
         //if (keywordFormatter->foundLine())
           {
             int sr2, sc2, dr2, dc2;
             keywordFormatter->getCommentBounds(sr2, sc2, dr2, dc2);
         //printf("K %d %d %d %d\n", sr2, sc2, dr2, dc2);
             if (sr2 < sr && sr2 != -1)
               sr = sr2;
             if (sc2 < sc && sc2 != -1)
               sc = sc2;
             if (dr2 > dr && dr2 != -1)
               dr = dr2;
             if (dc2 > dc && dc2 != -1)
               dc = dc2;
           }
         std::cout << "S " << sr << " " << sc << " " << dr << " " << dc << std::endl;
       }
   }
 infile.close();
 if (coutbuf)
   std::cout.rdbuf(coutbuf);

 return 0;
}

int
php_processor (std::string filename, std::string langfile, std::string outfilename, bool boilerplate, int stats)
{
 srchilite::RegexRuleFactory ruleFactory;
 srchilite::LangDefManager langDefManager (&ruleFactory);

 boost::shared_ptr<srchilite::HighlightState> p = NULL;
 try
   {
     char *dir = getenv ("LU_LANGFILE_DIR");
     if (dir)
       p = langDefManager.getHighlightState(dir, langfile);
     else
       p = langDefManager.getHighlightState(LICENSEUTILS_DATADIR "/lang-files",
                                            langfile);
   } catch (std::exception &ex)
   {
     fprintf (stderr," couldn't read langfile '%s'\n", langfile.c_str());
     exit (1);
   }
 srchilite::SourceHighlighter highlighter (p);

 string line;
 int lineNo = 0;
 ifstream infile;
 infile.open (filename.c_str());
 srchilite::FormatterManager formatterManager
         (PHPInfoFormatterPtr(new PHPInfoFormatter));
 PHPInfoFormatterPtr keywordFormatter(new PHPInfoFormatter("keyword"));
 formatterManager.addFormatter("keyword", keywordFormatter);

 PHPInfoFormatterPtr commentFormatter(new PHPInfoFormatter("comment"));
 formatterManager.addFormatter("comment", commentFormatter);

 PHPInfoFormatterPtr linkFormatter(new PHPInfoFormatter("url"));
 formatterManager.addFormatter("url", linkFormatter);

 PHPInfoFormatterPtr todoFormatter(new PHPInfoFormatter("todo"));
 formatterManager.addFormatter("todo", todoFormatter);

 PHPInfoFormatterPtr typeFormatter(new PHPInfoFormatter("type"));
 formatterManager.addFormatter("type", typeFormatter);

 PHPInfoFormatterPtr preprocFormatter(new PHPInfoFormatter("preproc"));
 formatterManager.addFormatter("preproc", preprocFormatter);
 highlighter.setFormatterManager(&formatterManager);

 // normally we stop when any of these find something.
 PHPInfoFormatterPtr ar[] =
   {
     keywordFormatter,
     preprocFormatter,
     NULL
   };
 srchilite::FormatterParams params;
 highlighter.setFormatterParams(&params);

 std::ofstream out(outfilename.size () > 0 ? outfilename.c_str() : "");
 std::streambuf *coutbuf = NULL;
 if (outfilename.size () > 0)
   {
     coutbuf = std::cout.rdbuf(); 
     std::cout.rdbuf(out.rdbuf());
   }
 // we now highlight a line a time
 while (getline(infile, line))
   {
     params.start = 0; // reset position counter within a line

     commentFormatter->setLineNo(lineNo);
     keywordFormatter->setLineNo(lineNo);
     try 
       {
         highlighter.highlightParagraph(line);
       } catch (std::exception &e)
     {
       //okay all bets are off
       break;
     }
     lineNo++;
     if (boilerplate)
       {
         if (stop_the_madness)
           break;
         bool found = false;
         int count = 0;
         while (ar[count])
           {
             if (ar[count]->foundLine())
               {
                 found = true;
                 break;
               }
             count++;
           }
         if (found)
           break;
       }
   }
 if (commentFormatter->foundLine())
   {
     std::cout << std::endl;
     if (stats)
       {
         int sr, sc, dr, dc;
         commentFormatter->getCommentBounds(sr, sc, dr, dc);
         //printf("C %d %d %d %d\n", sr, sc, dr, dc);
         //if (keywordFormatter->foundLine())
           {
             int sr2, sc2, dr2, dc2;
             keywordFormatter->getCommentBounds(sr2, sc2, dr2, dc2);
         //printf("K %d %d %d %d\n", sr2, sc2, dr2, dc2);
             if (sr2 < sr && sr2 != -1)
               sr = sr2;
             if (sc2 < sc && sc2 != -1)
               sc = sc2;
             if (dr2 > dr && dr2 != -1)
               dr = dr2;
             if (dc2 > dc && dc2 != -1)
               dc = dc2;
           }
         std::cout << "S " << sr << " " << sc << " " << dr << " " << dc << std::endl;
       }
   }
 infile.close();
 if (coutbuf)
   std::cout.rdbuf(coutbuf);

 return 0;
}

int
default_processor (std::string filename, std::string langfile, std::string outfilename, bool boilerplate, int stats)
{
 srchilite::RegexRuleFactory ruleFactory;
 srchilite::LangDefManager langDefManager (&ruleFactory);

 boost::shared_ptr<srchilite::HighlightState> p = NULL;
 try
   {
     char *dir = getenv ("LU_LANGFILE_DIR");
     if (dir)
       p = langDefManager.getHighlightState(dir, langfile);
     else
       p = langDefManager.getHighlightState(LICENSEUTILS_DATADIR "/lang-files",
                                            langfile);
   } catch (std::exception &ex)
   {
     fprintf (stderr," couldn't read langfile '%s'\n", langfile.c_str());
     exit (1);
   }
 srchilite::SourceHighlighter highlighter (p);

 string line;
 int lineNo = 0;
 ifstream infile;
 infile.open (filename.c_str());
 srchilite::FormatterManager formatterManager
         (InfoFormatterPtr(new InfoFormatter));
 InfoFormatterPtr keywordFormatter(new InfoFormatter("keyword"));
 formatterManager.addFormatter("keyword", keywordFormatter);

 InfoFormatterPtr commentFormatter(new InfoFormatter("comment"));
 formatterManager.addFormatter("comment", commentFormatter);

 InfoFormatterPtr linkFormatter(new InfoFormatter("url"));
 formatterManager.addFormatter("url", linkFormatter);

 InfoFormatterPtr todoFormatter(new InfoFormatter("todo"));
 formatterManager.addFormatter("todo", todoFormatter);

 //InfoFormatterPtr typeFormatter(new InfoFormatter("type"));
 //formatterManager.addFormatter("type", typeFormatter);

 InfoFormatterPtr preprocFormatter(new InfoFormatter("preproc"));
 formatterManager.addFormatter("preproc", preprocFormatter);

 highlighter.setFormatterManager(&formatterManager);

 // normally we stop when any of these find something.
 InfoFormatterPtr ar[] =
   {
     keywordFormatter,
     preprocFormatter,
     NULL
   };
 srchilite::FormatterParams params;
 highlighter.setFormatterParams(&params);

 std::ofstream out(outfilename.size () > 0 ? outfilename.c_str() : "");
 std::streambuf *coutbuf = NULL;
 if (outfilename.size () > 0)
   {
     coutbuf = std::cout.rdbuf(); 
     std::cout.rdbuf(out.rdbuf());
   }
 // we now highlight a line a time
 while (getline(infile, line))
   {
     params.start = 0; // reset position counter within a line

     commentFormatter->setLineNo(lineNo);
     keywordFormatter->setLineNo(lineNo);
     try 
       {
         highlighter.highlightParagraph(line);
       } catch (std::exception &e)
     {
       //okay all bets are off
       break;
     }
     lineNo++;
     if (boilerplate)
       {
         if (stop_the_madness)
           break;
         bool found = false;
         int count = 0;
         while (ar[count])
           {
             if (ar[count]->foundLine())
               {
                 found = true;
                 break;
               }
             count++;
           }
         if (found)
           break;
       }
   }
 if (commentFormatter->foundLine())
   {
     std::cout << std::endl;
     if (stats)
       {
         int sr, sc, dr, dc;
         commentFormatter->getCommentBounds(sr, sc, dr, dc);
         //printf("C %d %d %d %d\n", sr, sc, dr, dc);
         //if (keywordFormatter->foundLine())
           {
             int sr2, sc2, dr2, dc2;
             keywordFormatter->getCommentBounds(sr2, sc2, dr2, dc2);
         //printf("K %d %d %d %d\n", sr2, sc2, dr2, dc2);
             if (sr2 < sr && sr2 != -1)
               sr = sr2;
             if (sc2 < sc && sc2 != -1)
               sc = sc2;
             if (dr2 > dr && dr2 != -1)
               dr = dr2;
             if (dc2 > dc && dc2 != -1)
               dc = dc2;
           }
         std::cout << "S " << sr << " " << sc << " " << dr << " " << dc << std::endl;
       }
   }
 infile.close();
 if (coutbuf)
   std::cout.rdbuf(coutbuf);

 return 0;
}

int
main (int argc, char **argv)
{
  std::string filename;
  std::string langfile = "c.lang";
  std::string style = "c";
  std::string outfilename;
  int stats = 0;
  bool boilerplate = true;
  if (argc > 1)
    {
      for (int i = 2; i <= argc; i++)
	{
          std::string parameter(argv[i-1]); 
	  if (parameter == "-f" || parameter == "--full")
            boilerplate = false;
          else if (parameter == "-O" || parameter == "--output")
            {
	      i++;
              if (i - 1 >= argc)
		{
                  std::cerr << "Error: missing argument for --output" << std::endl;
		  exit (1);
                }
              outfilename = std::string(argv[i-1]);
            }
          else if (parameter == "--stats")
            stats = 1;
          else if (parameter == "--version")
            {
              std::cout << "lu-comment-extractor" << " " << VERSION << std::endl;
              exit (0);
            }
          else if (parameter == "-s" || parameter == "--style")
            {
	      i++;
              if (i - 1 >= argc)
		{
                  std::cerr << "Error: missing argument for --style" << std::endl;
		  exit (1);
                }
              char *s = argv[i-1];
              if (strcasecmp (s, "c") == 0)
                langfile = "c.lang";
              else if (strcasecmp (s, "c++") == 0)
                langfile = "cpp.lang";
              else if (strcasecmp (s, "javascript") == 0)
                langfile = "javascript.lang";
              else if (strcasecmp (s, "shell") == 0)
                langfile = "sh.lang";
              else if (strcasecmp (s, "scheme") == 0)
                langfile = "scheme.lang";
              else if (strcasecmp (s, "texinfo") == 0)
                langfile = "texinfo.lang";
              else if (strcasecmp (s, "latex") == 0)
                langfile = "latex.lang";
              else if (strcasecmp (s, "m4") == 0)
                langfile = "m4.lang";
              else if (strcasecmp (s, "haskell") == 0)
                langfile = "haskell.lang";
              else if (strcasecmp (s, "groff") == 0)
                langfile = "groff_man.outlang";
              else if (strcasecmp (s, "gettext") == 0)
                langfile = "sh.lang";
              else if (strcasecmp (s, "fortran") == 0)
                langfile = "fortran.lang";
              else if (strcasecmp (s, "pascal") == 0)
                langfile = "pascal.lang";
              else if (strcasecmp (s, "csharp") == 0)
                langfile = "csharp.lang";
              else if (strcasecmp (s, "css") == 0)
                langfile = "css.lang";
              else if (strcasecmp (s, "go") == 0)
                langfile = "go.lang";
              else if (strcasecmp (s, "java") == 0)
                langfile = "java.lang";
              else if (strcasecmp (s, "perl") == 0)
                langfile = "perl.lang";
              else if (strcasecmp (s, "php") == 0)
                langfile = "php.lang";
              else if (strcasecmp (s, "python") == 0)
                langfile = "python.lang";
              else if (strcasecmp (s, "r") == 0)
                langfile = "r.lang";
              else if (strcasecmp (s, "ruby") == 0)
                langfile = "ruby.lang";
              else if (strcasecmp (s, "scala") == 0)
                langfile = "scala.lang";
              else if (strcasecmp (s, "swift") == 0)
                langfile = "swift.lang";
              else
                {
                  cerr << "Error: unknown style " << std::string (s) << std::endl;
                  exit (1);
                }
            }
          else if (parameter == "--style=c")
              langfile = "c.lang";
          else if (parameter == "--style=c++")
            langfile = "cpp.lang";
          else if (parameter == "--style=javascript")
            langfile = "javascript.lang";
          else if (parameter == "--style=shell")
            langfile = "sh.lang";
          else if (parameter == "--style=scheme")
            langfile = "scheme.lang";
          else if (parameter == "--style=texinfo")
            langfile = "texinfo.lang";
          else if (parameter == "--style=latex")
            langfile = "latex.lang";
          else if (parameter == "--style=m4")
            langfile = "m4.lang";
          else if (parameter == "--style=haskell")
            langfile = "haskell.lang";
          else if (parameter == "--style=groff")
            langfile = "groff_man.outlang";
          else if (parameter == "--style=gettext")
            langfile = "sh.lang";
          else if (parameter == "--style=fortran")
            langfile = "fortran.lang";
          else if (parameter == "--style=pascal")
            langfile = "pascal.lang";
          else if (parameter ==  "--style=csharp")
            langfile = "csharp.lang";
          else if (parameter ==  "--style=css")
            langfile = "css.lang";
          else if (parameter ==  "--style=go")
            langfile = "go.lang";
          else if (parameter ==  "--style=java")
            langfile = "java.lang";
          else if (parameter ==  "--style=perl")
            langfile = "perl.lang";
          else if (parameter ==  "--style=php")
            langfile = "php.lang";
          else if (parameter ==  "--style=python")
            langfile = "python.lang";
          else if (parameter ==  "--style=r")
            langfile = "r.lang";
          else if (parameter ==  "--style=ruby")
            langfile = "ruby.lang";
          else if (parameter ==  "--style=scala")
            langfile = "scala.lang";
          else if (parameter ==  "--style=swift")
            langfile = "swift.lang";
	  else if (parameter == "--help" || parameter == "-h")
	    {
              std::cout << "Usage: " << std::string(argv[0]) << " [OPTION]... FILE" << std::endl;
              std::cout << "Show the topmost comments of source files (using GNU Source Highlight.)" << std::endl << std::endl;
              std::cout << "  -f, --full        instead of showing topmost comments, show them all" << std::endl;
              std::cout << "  -s, --style NAME  parse comments in the style of NAME" << std::endl;
              std::cout << "      --stats       also show starting and ending byte positions" << std::endl;
              std::cout << "  -O, --output FILE dump comments to FILE instead of stdout" << std::endl;
              std::cout << "  -h, --help        display this help and exit" << std::endl;
              std::cout << "      --version     display version information and exit" << std::endl;
              std::cout << std::endl;
              std::cout << "Style NAME can be one of \"c, c++, javascript, shell, scheme, texinfo, latex," << std::endl << "m4, haskell, groff, gettext, fortran, pascal\".  \"c\" is the default." << std::endl << std::endl;
              std::cout << "--stats produces a terminating line showing the source row, source column, " << std::endl << "along with the destination row, destination column of the detected comments." << std::endl << std::endl;
              std::cout << "Report bugs to" << " <" << PACKAGE_BUGREPORT ">" << " for v" << VERSION << "." << std::endl;
	      exit(0);
	    }
	  else
            {
              if (parameter.c_str()[0] == '-')
                {
                  std::cerr << "Error: unknown option '" << parameter << "'" << std::endl;
                  exit (1);
                }
              filename = parameter;
            }
	}
    }
  if (filename.size() == 0)
    {
      std::cout << "Usage: " << std::string(argv[0]) << " [OPTION]... FILE" << std::endl;
      exit (1);
    }
  if (langfile == "m4.lang")
    return m4_processor (filename, langfile, outfilename, boilerplate, stats);
  else if (langfile == "php.lang")
    return php_processor (filename, langfile, outfilename, boilerplate, stats);
  else
    return default_processor (filename, langfile, outfilename, boilerplate, stats);
 return 0;
}
