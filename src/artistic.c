/*  Copyright (C) 2017 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/
#include <config.h>
#include <stdlib.h>
#include <unistd.h>
#include "licensing_priv.h"
#include "artistic.h"
#include "gettext-more.h"
#include "xvasprintf.h"
#include "read-file.h"
#include "error.h"
#include "util.h"
#include "url-downloader.h"

static struct argp_option argp_options[] = 
{
    {"v1.0", '1', NULL, 0, N_("show version 1.0 of the license notice")},
    {"v2.0", '2', NULL, 0, N_("show version 2.0 of the license notice")},
    {"full", 'f', NULL, 0, N_("show the full license text")},
    {"list-license-notices", 'l', NULL, OPTION_HIDDEN, N_("show licenses and exit")},
    {0}
};

static error_t 
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct lu_artistic_options_t *opt = NULL;
  if (state)
    opt = (struct lu_artistic_options_t*) state->input;
  switch (key)
    {
    case 'f':
      opt->full_text = 1;
      break;
    case '1':
      opt->version=1; //verison 1.0
      break;
    case '2':
      opt->version=2; //version 2.0
      break;
    case 'l':
        {
          int i = 0;
          while (artistic.notices[i].keyword != NULL)
            {
              fprintf (stdout, "%s\n", artistic.notices[i].keyword);
              i++;
            }
          exit (0);
        }
      break;
    case ARGP_KEY_INIT:
      opt->version = 2;
      opt->full_text = 0;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

#undef ARTISTIC_DOC
#define ARTISTIC_DOC N_("Show the Artistic license.")
static struct argp argp = { argp_options, parse_opt, "", ARTISTIC_DOC};

int 
lu_artistic_parse_argp (struct lu_state_t *state, int argc, char **argv)
{
  int err = 0;
  struct lu_artistic_options_t opts;
  opts.state = state;

  err = argp_parse (&argp, argc, argv, state->argp_flags,  0, &opts);
  if (!err)
    return lu_artistic (state, &opts);
  else
    return err;
}

int
show_lu_artistic(struct lu_state_t *state, struct lu_artistic_options_t *options)
{
  // Unfortunately the artistic license doesn't have an authoritative
  // notice, so our best guess is the verbage what Larry Wall used in perl.c.
  if (!options->full_text)
    {
      switch (options->version)
        {
        case 1:
          luprintf (state,
                    N_("You may distribute this file under the terms of the Artistic License 1.0.\n"));
          break;
        default:
        case 2:
          luprintf (state,
                    N_("You may distribute this file under the terms of the Artistic License 2.0.\n"));
          break;
        }
      return 0;
    }
  char *url;
  if (options->version == 1)
    url = strdup ("https://directory.fsf.org/wiki/License:Artistic_v1.0");
  else if (options->version == 2)
    url = strdup ("https://directory.fsf.org/wiki/License:ArtisticLicense2.0");
  else 
    url = strdup ("https://directory.fsf.org/wiki/License:ArtisticLicense2.0");
  char *data = NULL;
  int err = download (state, url, &data);
  free (url);
  replace_html_entities (data);


  if (options->version == 1)
    err = show_lines_after (state, data, 
                            "The Artistic License\n", 
                            112, 0, NULL, NULL);
  else if (options->version == 2)
    err = show_lines_after (state, data, 
                            "\t	       The Artistic License 2.0\n", 
                            201, 0, NULL, NULL);
  else
    err = show_lines_after (state, data, 
                            "\t        The Artistic License 2.0\n", 
                            201, 0, NULL, NULL);
  free (data);
  return err;
}

int 
lu_artistic (struct lu_state_t *state, struct lu_artistic_options_t *options)
{
  int err = 0;
  err = show_lu_artistic (state, options);
  return err;
}

struct lu_command_t artistic = 
{
  .name         = N_("artistic"),
  .doc          = ARTISTIC_DOC,
  .flags        = IS_A_LICENSE | SAVE_IN_HISTORY | SHOW_IN_HELP,
  .argp         = &argp,
  .parser       = lu_artistic_parse_argp,
  .latest_idx   = 0,
  .mentions     =
    {
      " artistic license ",
      NULL
    },
  .notices      =
    {
        {
          .keyword   = "artisticv1,full-license",
          .cmd       = "artistic --full -1",
          .words     = "Artistic,artistic control",
          .recommend = 0
        },
        {
          .keyword   = "artisticv2,full-license",
          .cmd       = "artistic --full -2",
          .words     = "Artistic,2.0",
          .recommend = 1
        },
        {
          .keyword   = "artisticv2",
          .cmd       = "artistic -2",
          .words     = "Artistic,2.0",
          .recommend = 1
        },
        {
          .keyword   = "artisticv1",
          .cmd       = "artistic -1",
          .words     = "Artistic,1.0",
          .recommend = 0
        },
        {
          .keyword   = NULL,
          .cmd       = NULL,
          .words     = NULL,
          .recommend = 0
        },
    },
};
