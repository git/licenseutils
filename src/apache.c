/*  Copyright (C) 2013, 2014 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/
#include <config.h>
#include <stdlib.h>
#include <unistd.h>
#include "licensing_priv.h"
#include "apache.h"
#include "gettext-more.h"
#include "xvasprintf.h"
#include "read-file.h"
#include "error.h"
#include "util.h"
#include "url-downloader.h"
#include "argz.h"

static struct argp_option argp_options[] = 
{
  {"1.1", '1', NULL, 0, N_("show the license for version 1.1")},
  {"2.0", '2', NULL, 0, N_("show the license notice for version 2.0")},
  {"full", 'f', NULL, 0, N_("show the full license text (2.0)")},
  {"list-license-notices", 'l', NULL, OPTION_HIDDEN, N_("show licenses and exit")},
  {0}
};

static error_t 
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct lu_apache_options_t *opt = NULL;
  if (state)
    opt = (struct lu_apache_options_t*) state->input;
  switch (key)
    {
    case '1':
      opt->version = 1; //1.1
      break;
    case '2':
      opt->version = 2; //2.0
      break;
    case 'f':
      opt->full = 1;
      break;
    case 'l':
        {
          int i = 0;
          while (apache.notices[i].keyword != NULL)
            {
              fprintf (stdout, "%s\n", apache.notices[i].keyword);
              i++;
            }
          exit (0);
        }
      break;
    case ARGP_KEY_INIT:
      opt->full = 0;
      opt->version = 2;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

#undef APACHE_DOC
#define APACHE_DOC N_("Show the Apache License notice.")
static struct argp argp = { argp_options, parse_opt, "", APACHE_DOC};

int 
lu_apache_parse_argp (struct lu_state_t *state, int argc, char **argv)
{
  int err = 0;
  struct lu_apache_options_t opts;
  opts.state = state;

  err = argp_parse (&argp, argc, argv, state->argp_flags,  0, &opts);
  if (!err)
    return lu_apache (state, &opts);
  else
    return err;
}

int
show_lu_apache(struct lu_state_t *state, struct lu_apache_options_t *options)
{
  char *url;
  switch (options->version)
    {
    case 1:
      url = strdup ("https://www.apache.org/licenses/LICENSE-1.1");
      break;
    default:
    case 2:
      url = strdup ("http://directory.fsf.org/wiki/License:Apache2.0");
      break;
    }
  char *data = NULL;
  int err = download (state, url, &data);
  free (url);

  switch (options->version)
    {
    case 1:
        {
          char *argz = NULL;
          size_t len = 0;
          argz_add (&argz, &len, data);
          uncomment_comments (&argz, &len, "/*", "//", 0, 0, 1);
          argz_stringify (argz, len, '\n');
          luprintf (state, "%s\n", argz);
          free (argz);
        }
      break;
    default:
    case 2:
      replace_html_entities (data);
      if (options->full)
        err = show_lines_after (state, data, "<pre>", 202, 1, "<pre>", "");
      else
        err = show_lines_after (state, data,
                                "   Licensed under the Apache License",
                                11, 0, NULL, NULL);
      break;
    }
  free (data);
  return err;
}

int 
lu_apache (struct lu_state_t *state, struct lu_apache_options_t *options)
{
  int err = 0;
  err = show_lu_apache(state, options);
  return err;
}

struct lu_command_t apache = 
{
  .name         = N_("apache"),
  .doc          = APACHE_DOC,
  .flags        = IS_A_LICENSE | SAVE_IN_HISTORY | SHOW_IN_HELP,
  .argp         = &argp,
  .parser       = lu_apache_parse_argp,
  .latest_idx   = 2,
  .mentions     =
    {
      " Apache License",
      " apachev1.1 ",
      " apachev2.0 ",
      " apache v1.1 ",
      " apache v2.0 ",
      " apache version 1.1 ",
      " apache version 2.0 ",
      NULL
    },
  .notices      =
    {
        {
          .keyword   = "apachev1.1,full-license",
          .cmd       = "apache --full -1",
          .words     = "Apache,Version 1.1",
          .recommend = 0
        },
        {
          .keyword   = "apachev2,full-license",
          .cmd       = "apache --full -2",
          .words     = "Apache,Version 2.0",
          .recommend = 1
        },
        {
          .keyword   = "apachev2",
          .cmd       = "apache",
          .words     = "Apache,Version 2.0",
          .recommend = 1
        },
        {
          .keyword   = "apachev1.1",
          .cmd       = "apache -1",
          .words     = "Apache,Version 1.1",
          .recommend = 0
        },
        {
          .keyword   = NULL,
          .cmd       = NULL,
          .words     = NULL,
          .recommend = 0
        },
    }
};
