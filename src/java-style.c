/*  Copyright (C) 2017 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/
#include <config.h>
#include <stdlib.h>
#include <argz.h>
#include "read-file.h"
#include "gettext-more.h"
#include "java-style.h"
#include "util.h"

enum {
  OPT_JAVA = -123,
};

static struct argp_option argp_options[] = 
{
    {"java-style", OPT_JAVA, NULL, OPTION_HIDDEN, 
      N_("java style comments")},
    {0},
};

static error_t 
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct lu_comment_style_t **style = NULL;
  if (state)
    style = (struct lu_comment_style_t **) state->input;
  switch (key)
    {
    case OPT_JAVA:
      *style = &java_style;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

struct argp java_style_argp = { argp_options, parse_opt, "", "", 0};

static char *
comment (char *text)
{
  return create_line_comment (text, "//");
}

static void
uncomment (char **argz, size_t *len, int trim)
{
  uncomment_comments (argz, len, "/*", "//", trim, 0, 1);
  return;
}

static int
get_comment (char *file, char **argz, size_t *len, char **hashbang, int full, int *sr, int *sc, int *dr, int *dc)
{
  return get_comments (file, "java", argz, len, full, NULL, sr, sc, dr, dc);
}

struct lu_comment_style_t java_style=
{
  .name                = "java",
  .argp                = &java_style_argp,
  .get_initial_comment = get_comment,
  .comment             = comment,
  .uncomment           = uncomment,
  .multiline           = 1,
  .support_file_exts   = ".java",
  .avoid_file_exts     = NULL, // avoid no file extensions
};
